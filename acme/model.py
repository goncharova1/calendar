from datetime import datetime

class Note:
    id: str
    date: datetime
    title: str
    text: str